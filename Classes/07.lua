-- Importação de arquivo de audio
-- Carregando um arquivo de audio digital
keysdown = 0

function love.keypressed(key)
  if key ~= nil then
    keysdown = keysdown + 1
  end
    return keysdown;
end

function love.keyreleased(key)
    keysdown = keysdown - 1
end

function love.load()
  love.window.setMode(400, 400)
  meuAudio = love.audio.newSource("assets/robotness.mp3","static")-- carregar o audio
end

function love.draw()
  if love.keypressed() > 0 then
    love.audio.play(meuAudio);
  end
end

--[[
    O programa acima carrega um arquivo de audio, que é disparado pressionando
    qualquer tecla. O arquivo de audio está dentro da pasta "assets".
  ]]
