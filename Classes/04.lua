-- operações matemáticas
a = 1
b = 2
c = 3
d = 4

-- Adição, subtração, multiplicação, divisão
a = a+5
b = b-2
c = c*1.6
d = d/7

--[[
    A ordem das operações matemáticas ALTERA o produto. Numa operação matemática, 
    a multiplicação é sempre feita antes da soma. Para modificar essa ordem podem 
    ser usados parenteses. 
  ]]
x = 3 + 4 * 5

x = 23

x = (3 + 4) * 5

x = 35

-- Condições if, else, and, or

if x == 10 then
  -- Se a variavel x for igual a 10, realizar a operação entre "then" e "end"
end



if x == 10 then
  -- Se a variavel x for igual a 10, realizar a operação entre then e else
else
  -- se não, realizar a operação entre else e end
end

-- Condições AND / OR

if x > 600 and x < 800 then
  -- Se x for maior que 600 e se x for menor que 800, realizar a operação entrenthen e end
end

if x > 200 or x < 80 then
  -- Se x for maior que 200 ou se x for menor que 80, realizar a operação entre then e end
end

-- Input de mouse
x = 0
y = 0
function love.draw()
  x=love.mouse.getX()
  y=love.mouse.getY()
  love.graphics.ellipse("fill",x,y,30,30)

  if love.mouse.isDown(1,2,3) then
    -- se qualquer botão do mouse estiver pressionado
    love.graphics.ellipse("fill",20,20,5,5)
  end

  if love.mouse.isDown(1) then
    -- se botão esquerdo do mouse estiver pressionado
    love.graphics.ellipse("fill",20,180,5,5)
  end

  if love.mouse.isDown(2) then
    -- se botão esquerdo do mouse estiver pressionado
    love.graphics.ellipse("fill",180,180,5,5)
  end

end

--[[
  interação com o teclado
]]
keysdown = 0

function love.keypressed(key)
  if key ~= nil then
    keysdown = keysdown + 1
  end
    return keysdown;
end

function love.keyreleased(key)
    keysdown = keysdown - 1
end

function love.draw()
  if love.keypressed() > 0 then
    --Se alguma tecla for pressionada, realizar a operação entre then e end
  end
  
  if love.keyboard.isDown("a") == true then
    -- Se a tecla a for pressionada, realizar a operação.
  end
  
  if love.keyboard.isDown("up") == true then
    -- Se a seta para cima for pressionada, realizar a operação.
  end
end

-- Loop For
for i=0,100,2 do
  love.graphics.line(i,0,i,50)
end

--[[
    O valor inicial da variavel i é 0. O valor máximo de i é 100. o valor de i será 
    acrescido de 2 a cada loop. Enquanto i for menor que 100, realizar a operação 
    entre do e end.
  ]]