-- Desenhando com o módulo draw do love

-- Shapes básicos
-- POINT
  love.graphics.points(x,y)
  
-- LINE
  love.graphics.line(x1,y1,x2,y2)
  
-- TRIANGLE
  love.graphics.triangle(mode,x1,y1,x2,y2,x3,y3)
  
-- POLÍGONO
  love.graphics.polygon(mode,x1,y1,x2,y2,x3,y3,x4,y4)
  
-- RECT
  love.graphics.rectangle(mode,x,y,width,height)
  
-- ELLIPSE
  love.graphics.ellipse(mode,x,y,width,height)
  

-- Atributos de cor
-- BACKGROUND
  love.graphics.setBackgroundColor(red,green,blue) -- Cor de fundo.
  
-- FILL
  love.graphics.setColor(red,green,blue,alpha)-- Cor.
  
-- Atributos de traço
-- STROKE WEIGHT
  love.graphics.setLineWidth(pixels)-- Expessura do traço.
  