-- Importando uma imagem estática
function love.load()
  love.window.setMode(400,300)
  boca = love.graphics.newImage("assets/boca05.png")
end

function love.draw()
  love.graphics.draw(boca, 0, 0)
end

-- O programa acima carrega uma imagem e exibe na tela. A imagem PNG está na pasta assets

-- Usando fontes
function love.load()
  love.window.setMode(400,300)
  font = love.graphics.newFont("assets/Roboto-Light.ttf", 100)
  love.graphics.setFont(font);
end

function love.draw()
  love.graphics.print("Texto", 20, 20)
end

-- O programa acima carrega uma fonte e exibe um texto na tela.