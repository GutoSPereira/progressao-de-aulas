--[[
    Introdução à linguagem de programação Lua para designers
    
    Uma FUNÇÃO permite o desenho de formas, especificação de cores, executar cálculos 
    matemáticos	e vários	outros tipos de ação. O nome de uma função é geralmente uma 
    palavra seguida de parenteses. Os números entre parenteses são PARÂMETROS, e eles 
    afetam a maneira como a função trabalha.

    A função setMode tem, no mínimo, 2 parâmetros, que configuram a largura e a 
    altura da janela.
  ]]
  love.window.setMode(200, 200)

--[[
    A função setBackgroundColor aceita 3 parâmetros, que configuram a intensidade das cores
    Vermelho, Verde e Azul na especificação da cor de fundo
  ]]
  love.graphics.setBackgroundColor(255,0,0);

-- Um COMANDO é para a programação de computadores o equivalente a uma frase.

x = 102 -- A variável x é igual a 102.

love.window.setMode(200, 200)-- Crie uma tela com 200 pixels de largura e 200 pixels de altura.


--[[
    Variáveis

    Uma variável é um espaço do programa destinado a guardar dados digitais. Esses dados podem ser,
    por exemplo, números, caracteres, uma imagem digital ou a posição do mouse na tela.
  ]]
tamanho = 500;-- a variável tamanho tem seu valor de 500

bairro = "Jardim Botânico";-- a variável bairro tem em seu conteudo o texto 'Jardim Botânico'

x = x + 1 -- somar 1 ao valor de x

-- loop
-- LOOPS são usados para repetir uma parte do programa um determinado número de vezes.
y = 0
function love.draw()
  love.graphics.setBackgroundColor(255,255,0);
  love.graphics.line(0,y,300,y);
  y=y+2;

  for i=0, 9 do -- Enquanto a variavel i for menor ou igual a 9, desenhar uma linha do ponto i,20 até o ponto i,80.
    love.graphics.line(i,20,i,80)
  end

  -- condições
  --[[CONDIÇÕES permitem que um programa tome decisões a respeito de quais linhas do código rodarão e quais não. Elas 
    fazem com que ações aconteçam somente quando uma condição específica é alcançada. Condições permitem que um 
    programa se comporte de maneira diferente dependendo dos valores guardados nas suas variáveis.
    ]]
  if y > 100 then
    love.graphics.ellipse("fill",50,50,10,10)
  end
end